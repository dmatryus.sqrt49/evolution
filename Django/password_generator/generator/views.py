import random
import string
import random

from django.shortcuts import render
from django.http import HttpResponse


# Create your views here.


def home(request):
    return render(request, "generator/home.html")


def password(request):
    characters = string.ascii_lowercase
    if request.GET.get("uppercase"):
        characters += string.ascii_uppercase

    if request.GET.get("numbers"):
        characters += string.digits

    if request.GET.get("special"):
        characters += string.punctuation

    generated_password = "".join(
        [random.choice(characters) for _ in range(int(request.GET.get("length")))]
    )

    return render(request, "generator/password.html", {"password": generated_password})
